<?php

namespace App\Http\Controllers;

use App\Models\User;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;

class PurchaseOrderController extends Controller
{
    public function index()
    {
        return view('pages.purchase-order.index');
    }

    public function datatables()
    {
        return Laratables::recordsOf(User::class);
    }
}
