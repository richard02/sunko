<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PurchaseOrderController;

Route::group(['prefix' => 'purchase-order'], function () {
    Route::get('', [PurchaseOrderController::class, 'index']);
    Route::get('/datatables', [PurchaseOrderController::class, 'datatables']);
    Route::view('/master-list', 'pages.purchase-order.master-list.index');
    Route::view('/suppliers', 'pages.purchase-order.suppliers.index');
});

Route::view('/', 'pages.home.index');
Route::view('/home', 'pages.home.index');
Route::view('/receiving', 'pages.receiving.index');
Route::view('/production', 'pages.production.index');
Route::view('/sales-order', 'pages.sales-order.index');
Route::view('/sales-order/master-list', 'pages.sales-order.master-list.index');
Route::view('/sales-order/clients', 'pages.sales-order.clients.index');
Route::view('/machines', 'pages.machines.index');
Route::view('/reports', 'pages.reports.index');
Route::view('/reports/production-report', 'pages.reports.production-report.index');
Route::view('/reports/costing', 'pages.reports.costing.index');
Route::view('/reports/sales-report', 'pages.reports.sales-report.index');
Route::view('/reports/inventory-report', 'pages.reports.inventory-report.index');
Route::view('/reports/expense-report', 'pages.reports.expense-report.index');
Route::view('/inventory-monitoring', 'pages.inventory-monitoring.index');
Route::view('/labor-cost', 'pages.labor-cost.index');
Route::view('/supplier-comparison', 'pages.supplier-comparison.index');
Route::view('/accounting', 'pages.accounting.index');
