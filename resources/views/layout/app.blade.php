<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Website</title>
    <link rel="icon" href="{{ asset('assets/images/sunko_icon.png') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    @yield('css_after')
</head>
<body>

<div id="app">
    @yield('modals')
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <div class="mdl-layout-spacer"></div>
                <div class="top_nav">
                    <div class="d-flex align-items-center">
                        <div class="mr-3">
                            <a class="d-flex align-items-center" href="#">
                                <i class="far fa-user-circle mr-2"></i>
                                <span>Juan Dela Cruz</span>
                            </a>
                        </div>
                        <div class="mr-3">
                            <a href="#">
                                <i class="fas fa-cog"></i>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <i class="fas fa-sign-out-alt"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="mdl-layout__drawer">
            <div class="logo text-center white">
                <img src="{{ asset('assets/images/sunko_logo.png') }}" alt="" class="w-100">
            </div>
            <nav class="mdl-navigation flex-grow-1">
                <div class="mdl-navigation__link" id="home">
                    <a href="{{ url('/home') }}" class="wrapper">
                        <i class="fas fa-home"></i>
                        Home
                    </a>
                </div>
                <div class="mdl-navigation__link" id="purchase_order">
                    <a href="{{ url('/purchase-order') }}" class="wrapper d-flex justify-content-between align-items-center has_child">
                        <div>
                            <i class="fas fa-file-invoice-dollar"></i>
                            Purchase Order
                        </div>
                        <i class="fas fa-chevron-down dd"></i>
                    </a>
                    <div class="collapse" id="purchaseOrderCollapse">
                        <nav>
                            <a href="{{ url('/purchase-order/mater-list') }}" class="collapse_link">Master List</a>
                            <a href="{{ url('/purchase-order/suppliers') }}" class="collapse_link">Suppliers</a>
                        </nav>
                    </div>
                </div>
                <div class="mdl-navigation__link" id="receiving">
                    <a href="{{ url('/receiving') }}" class="wrapper">
                        <i class="fas fa-truck-loading"></i>
                        Receiving
                    </a>
                </div>
                <div class="mdl-navigation__link" id="production">
                    <a href="{{ url('/production') }}" class="wrapper">
                        <i class="fas fa-layer-group"></i>
                        Production
                    </a>
                </div>
                <div class="mdl-navigation__link" id="sales_order">
                    <a href="{{ url('/sales-order') }}" class="wrapper d-flex justify-content-between align-items-center has_child">
                        <div>
                            <i class="far fa-chart-bar"></i>
                            Sales Order
                        </div>
                        <i class="fas fa-chevron-down dd"></i>
                        <div class="collapse" id="salesOrderCollapse">
                            <nav>
                                <a href="{{ url('/sales-order/master-list') }}" class="collapse_link">Master List</a>
                                <a href="{{ url('/sales-order/clients') }}" class="collapse_link">Clients</a>
                            </nav>
                        </div>
                    </a>
                </div>
                <div class="mdl-navigation__link" id="machines">
                    <a href="{{ url('/machines') }}" class="wrapper">
                        <i class="fas fa-print"></i>
                        Machines
                    </a>
                </div>
                <div class="mdl-navigation__link" id="reports">
                    <a href="{{ url('/reports') }}" class="wrapper d-flex justify-content-between align-items-center has_child">
                        <div>
                            <i class="fas fa-file-alt"></i>
                            Reports
                        </div>
                        <i class="fas fa-chevron-down dd"></i>
                        <div class="collapse" id="reportsCollapse">
                            <nav>
                                <a href="{{ url('/reports/production-report') }}" class="collapse_link">Production Report</a>
                                <a href="{{ url('/reports/costing') }}" class="collapse_link">Costing</a>
                                <a href="{{ url('/reports/sales-report') }}" class="collapse_link">Sales Report</a>
                                <a href="{{ url('/reports/inventory-report') }}" class="collapse_link">Inventory Report</a>
                                <a href="{{ url('/reports/expense-report') }}" class="collapse_link">Expense Report</a>
                            </nav>
                        </div>
                    </a>
                </div>
                <div class="mdl-navigation__link" id="inventory_monitoring">
                    <a href="{{ url('/inventory-monitoring') }}" class="wrapper">
                        <i class="fas fa-boxes"></i>
                        Inventory Monitoring
                    </a>
                </div>
                <div class="mdl-navigation__link" id="labor_cost">
                    <a href="{{ url('/labor-cost') }}" class="wrapper">
                        <i class="fas fa-hand-holding-usd"></i>
                        Labor Cost
                    </a>
                </div>
                <div class="mdl-navigation__link" id="supplier_comparison">
                    <a href="{{ url('/supplier-comparison') }}" class="wrapper">
                        <i class="fas fa-exchange-alt"></i>
                        Supplier Comparison
                    </a>
                </div>
                <div class="mdl-navigation__link" id="accounting">
                    <a href="{{ url('/accounting') }}" class="wrapper">
                        <i class="fas fa-coins"></i>
                        Accounting
                    </a>
                </div>
            </nav>
        </div>
        <main class="mdl-layout__content">
            <div class="page-content">

                @yield('content')

            </div>
        </main>
    </div>
</div>

</body>

<script src="{{ asset('assets/js/app.js') }}"></script>
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@yield('js_after')
</html>
