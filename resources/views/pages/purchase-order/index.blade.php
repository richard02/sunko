@extends('layout.app')
@section('title')
    Purchase Order
@endsection

@section('css_after')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection
@section('modals')

    <div class="modal fade" id="addEntryModal" tabindex="-1" role="dialog" aria-labelledby="addEntryTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-uppercase mx-auto" id="addEntryTitle">Add Purchase Order</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <i class="material-icons">close</i>
                        </span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="pt-4">
                        <form action="#">

                            <div class="mb-4">
                                <div class="form-group mb-3">
                                    <div class="input_wrapper with_icon">
                                        <label for="chooseSupplier">Choose Your Supplier</label>
                                        <input type="text" class="form-control w-100" id="chooseSupplier" readonly>
                                        <i class="material-icons icon openSupplierList" data-toggle="tooltip" data-html="true" title="Supplier List" data-placement="top">more_horiz</i>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="input_wrapper with_icon">
                                        <label for="deliveryDate">Delivery Date</label>
                                        <input type="text" class="form-control w-100" id="deliveryDate">
                                        <i class="far fa-calendar-alt icon" data-toggle="tooltip" data-html="true" title="Supplier List" data-placement="top"></i>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="input_wrapper">
                                        <label for="paymentDeadline">Payment Deadline</label>
                                        <input type="text" class="form-control w-100" id="paymentDeadline">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <div class="table-responsive">
                                        <table class="table modal-table table-primary">
                                            <thead>
                                                <tr>
                                                    <th>Raw Material</th>
                                                    <th>Units</th>
                                                    <th>Price/Unit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Paper Roll</td>
                                                    <td>
                                                        <div class="input_wrapper no_label">
                                                            <input type="number" class="form-control text-center" min="1" value="2">
                                                        </div>
                                                    </td>
                                                    <td>100.00</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="3">
                                                        <div class="d-flex justify-content-between">
                                                            <div>
                                                                <a class="d-flex align-items-center p-0 text-secondary hand addRawMaterial">
                                                                    <i class="material-icons mr-2">add</i>
                                                                    Add Raw Material
                                                                </a>
                                                            </div>
                                                            <div>Total Amount: 200</div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-circular text-uppercase position-relative mdl-js-button mdl-js-ripple-effect">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="supplierList" tabindex="-1" role="dialog" aria-labelledby="supplierListTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-uppercase mx-auto" id="supplierListTitle">Supplier List</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <i class="material-icons">close</i>
                        </span>
                    </button>
                </div>
                <div class="modal-body">

                    <div>
                        <form action="#">

                            <div class="mb-4">
                                <div class="form-group mb-3">
                                    <div class="input_wrapper with_icon">
                                        <label for="searchSupplier">Search for Supplier</label>
                                        <input type="text" class="form-control w-100" id="searchSupplier">
                                        <i class="material-icons icon" data-toggle="tooltip" data-html="true" title="Search" data-placement="top">search</i>
                                    </div>
                                </div>
                            </div>
                            <div class="pl-3">
                                <div class="form-group">
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" id="supplierName1" name="supplierName" class="custom-control-input">
                                        <label class="custom-control-label hand" for="supplierName1">Suppler name 1</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" id="supplierName2" name="supplierName" class="custom-control-input">
                                        <label class="custom-control-label hand" for="supplierName2">Suppler name 2</label>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-circular text-uppercase position-relative mdl-js-button mdl-js-ripple-effect">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addRawMaterialModal" tabindex="-1" role="dialog" aria-labelledby="addRawMaterialTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-uppercase mx-auto" id="addRawMaterialTitle">Add Raw Material</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <i class="material-icons">close</i>
                        </span>
                    </button>
                </div>
                <div class="modal-body">

                    <div>
                        <form action="#">

                            <div class="mb-4">
                                <div class="form-group mb-3">
                                    <div class="input_wrapper with_icon">
                                        <label for="searchRawMaterial">Search for Raw Material</label>
                                        <input type="text" class="form-control w-100" id="searchRawMaterial">
                                        <i class="material-icons icon" data-toggle="tooltip" data-html="true" title="Search" data-placement="top">search</i>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="input_wrapper">
                                        <label for="searchRawMaterial">Category</label>
                                        <select class="form-control w-100">
                                            <option>Default select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="pl-3">
                                <div class="form-group">
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" id="rawMaterialName1" name="rawMaterialName" class="custom-control-input">
                                        <label class="custom-control-label hand" for="rawMaterialName1">Raw Material 1</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" id="rawMaterialName2" name="rawMaterialName" class="custom-control-input">
                                        <label class="custom-control-label hand" for="rawMaterialName2">Raw Material 2</label>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-circular text-uppercase position-relative mdl-js-button mdl-js-ripple-effect">Submit</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('content')

    <div class="p-4">

        <button class="add_entry_fab" data-toggle="modal" data-target="#addEntryModal">
            <div class="d-flex wrapper align-items-center">
                <i class="material-icons mr-2">add</i>
                <span class="mt-1">ADD NEW ENTRY</span>
            </div>
        </button>

        <div class="main_content">
            <div class="table-responsive table-shadow">
                <table class="table modal-table table-primary">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>PO Number</th>
                        <th>Supplier</th>
                        <th>Delivery Date</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @for ($i = 1; $i <= 10; $i++)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>2020010{{ $i }}</td>
                                <td>Best Buy</td>
                                <td>2020-01-{{ $i }}</td>
                                <td>2020-01-{{ $i }}</td>
                                <td>Pending</td>
                                <td>
                                    <div class="d-flex justify-content-end pr-3 position-relative">
                                        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon position-relative" id="tbl_menu_{{ $i }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="tbl_menu_{{ $i }}">
                                            <a class="dropdown-item" href="#">Download PDF</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="#">Delete</a>
                                            <a class="dropdown-item" href="#">Complete PO</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endfor
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="7">

                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('js_after')
    <script>
        $('#purchase_order').addClass('active')
        $('#purchaseOrderCollapse').addClass('show active')

        let addEntryModal   = $('#addEntryModal')
        supplierList    = $('#supplierList')
        rawMaterial     = $('#addRawMaterialModal')
        $('.openSupplierList').on('click', function () {
            addEntryModal.modal('hide')
            supplierList.modal('show')
            supplierList.on('hide.bs.modal', function () {
                addEntryModal.modal('show')
            })
        })
        $('.addRawMaterial').on('click', function () {
            addEntryModal.modal('hide')
            rawMaterial.modal('show')
            rawMaterial.on('hide.bs.modal', function () {
                addEntryModal.modal('show')
            })
        })
    </script>
@endsection
